Identifying Proceedings in WoS
================
Stephan Stahlschmidt
07 April, 2020

  - [Documentation from Clarivate](#documentation-from-clarivate)
      - [Do we observe the same ut\_eid with different
        doctypes?](#do-we-observe-the-same-ut_eid-with-different-doctypes)
  - [Which items are assigned to both indices SCI and
    CPCI?](#which-items-are-assigned-to-both-indices-sci-and-cpci)
      - [Where are Proceedings Papers
        located?](#where-are-proceedings-papers-located)
      - [Where are Articles located?](#where-are-articles-located)
  - [KB Processing Policy](#kb-processing-policy)
  - [Internal DZHW Policy](#internal-dzhw-policy)
  - [Summary](#summary)

# Documentation from Clarivate

(<https://images.webofknowledge.com/WOK48B3/help/WOS/hs_document_type.html>)

Definition *article*

> Reports of research on original works. Includes research papers,
> features, brief communications, case reports, technical notes,
> chronology, and full papers that were published in a journal and/or
> presented at a symposium or conference.

Definition *review*

> A renewed study of material previously studied. Includes review
> articles and surveys of previously published literature. Usually will
> not present any new information on a subject.

Defintion *Proceedings Paper*

> Published literature of conferences, symposia, seminars, colloquia,
> workshops, and conventions in a wide range of disciplines. Generally
> published in a book of conference proceedings.

On assigning two document types to same item:

> Some records in Web of Science may have two document types: Article
> and Proceedings Paper. An Article is generally published in a journal.
> A Proceedings Paper is generally published in a book of confernece
> proceedings. Records covered in the two Conference Proceedings indexes
> (CPCI-S and CPCI-SSH) are identified as Proceedings Paper. The same
> records covered in the three indexes (SCI-E, SSCI, and A\&HCI) are
> identified as Article when published in a journal.

How can we identify those double entries to not count the respective
content twice in our reporting exercises?

## Do we observe the same ut\_eid with different doctypes?

``` sql
SELECT ut_eid, COUNT(*)
FROM wos_b_2019.items
WHERE pubyear BETWEEN 2000 AND 2009
    AND doctype IN ('Article','Reviews','Proceedings Paper')
GROUP BY ut_eid
ORDER BY COUNT(*) DESC
```

| UT\_EID         | COUNT(\*) |
| :-------------- | --------: |
| 000209697400004 |         1 |
| 000186991000024 |         1 |
| 000175889600009 |         1 |
| 000186991000007 |         1 |
| 000168680000004 |         1 |

Displaying records 1 - 5

Apparently ut\_eid are included only once in `items`. Hence for
Clarivate’s statement to hold some `ut_eid` might be assigned to the
proceedings index `CPCI` and the journal index `SCI`, but how can we
pinpoint those?

# Which items are assigned to both indices SCI and CPCI?

Given the stated purpose of the indices, `Article` should be primary be
index in the CPI, while `Proceedings Paper` should be primary found in
the CPCI.

## Where are Proceedings Papers located?

``` sql
SELECT edition_value, pubtype, doctype, COUNT(*)
FROM wos_b_2019.items it
JOIN wos_b_2019.databasecollection dbc
    ON it.pk_items = dbc.fk_items
WHERE pubyear BETWEEN 2000 AND 2009
    AND doctype = 'Proceedings Paper'
GROUP BY edition_value, pubtype, doctype
ORDER BY COUNT(*) DESC
```

| EDITION\_VALUE | PUBTYPE        | DOCTYPE           | COUNT(\*) |
| :------------- | :------------- | :---------------- | --------: |
| WOS.ISTP       | Book in series | Proceedings Paper |    980626 |
| WOS.ISTP       | Book           | Proceedings Paper |    886129 |
| WOS.ISSHP      | Book           | Proceedings Paper |    105694 |
| WOS.ISSHP      | Book in series | Proceedings Paper |     61276 |
| WOS.ISTP       | Journal        | Proceedings Paper |      4813 |
| WOS.ISSHP      | Journal        | Proceedings Paper |       295 |

6 records

### Are Proceedings Papers from CPCI co-indexed in other indices?

``` sql
SELECT edition_value, pubtype, doctype, COUNT(*)
FROM wos_b_2019.items it
JOIN wos_b_2019.databasecollection dbc
    ON it.pk_items = dbc.fk_items
WHERE pubyear BETWEEN 2000 AND 2009
    AND ut_eid IN(
        SELECT ut_eid
        FROM wos_b_2019.items
        JOIN wos_b_2019.databasecollection dbc
           ON it.pk_items = dbc.fk_items
        WHERE doctype = 'Proceedings Paper'
            AND edition_value IN ('WOS.ISTP','WOS.ISSHP')
            AND pubyear BETWEEN 2000 AND 2009
    )
GROUP BY edition_value, pubtype, doctype
ORDER BY COUNT(*) DESC
```

| EDITION\_VALUE | PUBTYPE        | DOCTYPE           | COUNT(\*) |
| :------------- | :------------- | :---------------- | --------: |
| WOS.ISTP       | Book in series | Proceedings Paper |    980626 |
| WOS.ISTP       | Book           | Proceedings Paper |    886129 |
| WOS.ISSHP      | Book           | Proceedings Paper |    105694 |
| WOS.ISSHP      | Book in series | Proceedings Paper |     61276 |
| WOS.ISTP       | Journal        | Proceedings Paper |      4813 |
| WOS.ISSHP      | Journal        | Proceedings Paper |       295 |

6 records

Hence, `Proceedings Paper` are exclusively indexed in the CPCI.

## Where are Articles located?

``` sql
SELECT edition_value, pubtype, doctype, COUNT(*)
FROM wos_b_2019.items it
JOIN wos_b_2019.databasecollection dbc
    ON it.pk_items = dbc.fk_items
WHERE pubyear BETWEEN 2000 AND 2009
    AND doctype = 'Article'
GROUP BY edition_value, pubtype, doctype
ORDER BY COUNT(*) DESC
```

| EDITION\_VALUE | PUBTYPE        | DOCTYPE | COUNT(\*) |
| :------------- | :------------- | :------ | --------: |
| WOS.SCI        | Journal        | Article |   8259852 |
| WOS.SSCI       | Journal        | Article |    944361 |
| WOS.ISTP       | Journal        | Article |    809009 |
| WOS.AHCI       | Journal        | Article |    322363 |
| WOS.IC         | Journal        | Article |    167760 |
| WOS.SCI        | Book in series | Article |    163369 |
| WOS.ISTP       | Book in series | Article |    155292 |
| WOS.CCR        | Journal        | Article |     84077 |
| WOS.ISSHP      | Journal        | Article |     71399 |
| WOS.BSCI       | Book in series | Article |      5082 |
| WOS.ISSHP      | Book in series | Article |      4517 |
| WOS.SSCI       | Book in series | Article |      3337 |
| WOS.BHCI       | Book in series | Article |      2965 |
| WOS.BSCI       | Book           | Article |      2388 |
| WOS.BHCI       | Book           | Article |      1661 |
| WOS.ESCI       | Journal        | Article |      1055 |
| WOS.AHCI       | Book in series | Article |       709 |
| WOS.BHCI       | Journal        | Article |       215 |
| WOS.BSCI       | Journal        | Article |        55 |
| WOS.SCI        | Book           | Article |        54 |

Displaying records 1 - 20

`Article` are mostly covered by `Journal` and to some degree by `Book in
series`.

### Are journal articles in SCI co-indexed in other indices?

``` sql
SELECT edition_value, pubtype, doctype, COUNT(*)
FROM wos_b_2019.items it
JOIN wos_b_2019.databasecollection dbc
    ON it.pk_items = dbc.fk_items
WHERE pubyear BETWEEN 2000 AND 2009
    AND ut_eid IN(
        SELECT ut_eid
        FROM wos_b_2019.items
        JOIN wos_b_2019.databasecollection dbc
           ON it.pk_items = dbc.fk_items
        WHERE pubtype = 'Journal'
            AND doctype = 'Article'
            AND edition_value = 'WOS.SCI'
            AND pubyear BETWEEN 2000 AND 2009
    )
GROUP BY edition_value, pubtype, doctype
ORDER BY COUNT(*) DESC
```

| EDITION\_VALUE | PUBTYPE | DOCTYPE | COUNT(\*) |
| :------------- | :------ | :------ | --------: |
| WOS.SCI        | Journal | Article |   8259852 |
| WOS.ISTP       | Journal | Article |    809009 |
| WOS.SSCI       | Journal | Article |    337623 |
| WOS.IC         | Journal | Article |    167760 |
| WOS.CCR        | Journal | Article |     84077 |
| WOS.ISSHP      | Journal | Article |     16203 |
| WOS.AHCI       | Journal | Article |     16054 |
| WOS.BSCI       | Journal | Article |        55 |
| WOS.BHCI       | Journal | Article |        25 |

9 records

Nearly 10% of SCI indexed journal articles are also included in the
CPCI.

### Are there any journal articles exclusively indexed in the CPCI?

Counting all entires in *items* table, entries for *SCI*, *SSCI* and
*AHCI* and exclusive entries for *ISSH* and
*ISTP*:

``` sql
SELECT AHCI_SSCI_SCI.pubyear, IT_CNT.items_cnt, AHCI_SSCI_SCI_cnt, ISSHP_ISTP.ISSHP_ISTP_exclusive_cnt
FROM (
    SELECT pubyear, COUNT(DISTINCT pk_items) AHCI_SSCI_SCI_cnt -- article from journal in AHCI, SSCI and SCI
    FROM wos_b_2019.items it
    JOIN wos_b_2019.databasecollection dbc ON it.pk_items = dbc.fk_items
    WHERE pubyear BETWEEN 2000 AND 2009
        AND edition_value IN ('WOS.AHCI','WOS.SSCI','WOS.SCI')
        AND doctype = 'Article'
        AND pubtype = 'Journal'
    GROUP BY pubyear
) AHCI_SSCI_SCI
JOIN (
    SELECT pubyear, COUNT(DISTINCT pk_items) items_cnt -- article from journal in items table
    FROM wos_b_2019.items it
    WHERE pubyear BETWEEN 2000 AND 2009
        AND doctype = 'Article'
        AND pubtype = 'Journal'
    GROUP BY pubyear
) IT_CNT ON IT_CNT.pubyear = AHCI_SSCI_SCI.pubyear
LEFT JOIN (
    SELECT pubyear, COUNT(DISTINCT pk_items) ISSHP_ISTP_exclusive_cnt -- exclusive article from journal in ISSHP and ISTP
    FROM wos_b_2019.items it
    JOIN wos_b_2019.databasecollection dbc ON it.pk_items = dbc.fk_items
    WHERE pubyear BETWEEN 2000 AND 2009
        AND edition_value IN ('WOS.ISSHP','WOS.ISTP')
        AND doctype = 'Article'
        AND pubtype = 'Journal'
        AND pk_items NOT IN ( -- we want only additional journal articles not already indexed in AHCI, SSCI or SCI
            SELECT DISTINCT pk_items
            FROM wos_b_2019.items it
            JOIN wos_b_2019.databasecollection dbc ON it.pk_items = dbc.fk_items
            WHERE pubyear BETWEEN 2000 AND 2009
                AND edition_value IN ('WOS.AHCI','WOS.SSCI','WOS.SCI')
                AND doctype = 'Article'
                AND pubtype = 'Journal'
        )
    GROUP BY pubyear
) ISSHP_ISTP ON ISSHP_ISTP.pubyear = AHCI_SSCI_SCI.pubyear
ORDER BY pubyear ASC
```

| PUBYEAR | ITEMS\_CNT | AHCI\_SSCI\_SCI\_CNT | ISSHP\_ISTP\_EXCLUSIVE\_CNT |
| :------ | ---------: | -------------------: | --------------------------: |
| 2000    |     784291 |               784291 |                          NA |
| 2001    |     786020 |               786020 |                          NA |
| 2002    |     796397 |               796397 |                          NA |
| 2003    |     823480 |               823480 |                          NA |
| 2004    |     855523 |               855523 |                          NA |
| 2005    |     895538 |               895484 |                          NA |
| 2006    |     948454 |               948336 |                          NA |
| 2007    |    1022024 |              1021898 |                          NA |
| 2008    |    1092929 |              1092609 |                          NA |
| 2009    |    1136737 |              1136491 |                          NA |

10 records

Currently there are no journal articles exclusively indexed in the CPCI.

### Are there any other journal articles?

Counting number of *Article* in *Journal* present in the *items* table,
but not included in our purchased indices *SCI*, *SSCI*, *AHCI*, *ISSH*
or *ISTP*:

``` sql
SELECT edition_value, COUNT(DISTINCT pk_items)
FROM wos_b_2019.items it
JOIN wos_b_2019.databasecollection dbc ON it.pk_items = dbc.fk_items
WHERE pubyear = 2017
    AND doctype = 'Article'
    AND pubtype = 'Journal'
    AND pk_items NOT IN (
        SELECT DISTINCT pk_items
        FROM wos_b_2019.items it
        JOIN wos_b_2019.databasecollection dbc ON it.pk_items = dbc.fk_items
        WHERE pubyear = 2017
            AND edition_value IN ('WOS.AHCI','WOS.SSCI','WOS.SCI', 'WOS.ISSHP','WOS.ISTP')
            AND doctype = 'Article'
            AND pubtype = 'Journal'
    )
GROUP BY edition_value
```

| EDITION\_VALUE | COUNT(DISTINCTPK\_ITEMS) |
| :------------- | -----------------------: |
| WOS.ESCI       |                    51967 |

1 records

Apparently these were indexed erroneously in the KB systems by ingesting
complete correction files, as these corrections also contained UTs not
covered by the KB contract with Clarivate (see E-Mail exchange
Clarivate, FIZ and DZHW from March/April
2020).

### Are articles from book in series from SCI co-indexed in other indices?

``` sql
SELECT edition_value, pubtype, doctype, COUNT(*)
FROM wos_b_2019.items it
JOIN wos_b_2019.databasecollection dbc
    ON it.pk_items = dbc.fk_items
WHERE pubyear BETWEEN 2000 AND 2009
    AND ut_eid IN(
        SELECT ut_eid
        FROM wos_b_2019.items
        JOIN wos_b_2019.databasecollection dbc
           ON it.pk_items = dbc.fk_items
        WHERE pubtype = 'Book in series'
            AND doctype = 'Article'
            AND edition_value = 'WOS.SCI'
            AND pubyear BETWEEN 2000 AND 2009
    )
GROUP BY edition_value, pubtype, doctype
ORDER BY COUNT(*) DESC
```

| EDITION\_VALUE | PUBTYPE        | DOCTYPE | COUNT(\*) |
| :------------- | :------------- | :------ | --------: |
| WOS.SCI        | Book in series | Article |    163369 |
| WOS.ISTP       | Book in series | Article |    155225 |
| WOS.ISSHP      | Book in series | Article |      3544 |
| WOS.BSCI       | Book in series | Article |      1107 |
| WOS.SSCI       | Book in series | Article |       803 |
| WOS.BHCI       | Book in series | Article |        13 |

6 records

95% of all `Article` in `Book in series` from the `SCI` are also indexed
as such in the `CPCI`.

# KB Processing Policy

`Proceedings Paper` are solely available via the CPCI. However, several
`Article` in `Journal` are also linked to the CPCI. Those seem to hold
two document types (`Article` and `Proceedings Paper`), as indicated by
Clarivate’s definition:

``` sql
SELECT ut_eid
FROM wos_b_2019.items it
JOIN wos_b_2019.databasecollection dbc
   ON it.pk_items = dbc.fk_items
WHERE pubtype = 'Journal'
    AND doctype = 'Article'
    AND edition_value = 'WOS.SCI'
    AND pubyear = 2009
    AND ut_eid IN(
        SELECT ut_eid
        FROM wos_b_2019.items it
        JOIN wos_b_2019.databasecollection dbc
           ON it.pk_items = dbc.fk_items
        WHERE pubtype = 'Journal'
            AND doctype = 'Article'
            AND edition_value = 'WOS.ISTP'
            AND pubyear = 2009)
```

| UT\_EID         |
| :-------------- |
| 000269779500002 |

Displaying records 1 - 1

Respective XML excerpt:

``` xml
<doctypes count="2">
  <doctype>Article</doctype>
  <doctype>Proceedings Paper</doctype>
</doctypes>
```

The same observation holds also for `Article` in `Book in series` from
the SCI. Here also both document types are included in the original XML
file, but the `Proceedings Paper` type is discarded in the KB processing
and onyl the `Article` one is transferred to the Bibliometriedatenbank.

# Internal DZHW Policy

> *pk\_items* defined as an article and at the same time a proceeding
> should be included in any analysis of *Article* from *Journal*. This
> might be done via controlling for *pubtype = ‘Journal’* and *doctype =
> ‘Article’*. Pure proceedings without any overlap might be identified
> via *doctype = ‘Proceedings Paper’*.

(Source: E-Mail Paul Donner 20180817, see folder *50\_Instrumente* in KB
folder on Q drive.)

# Summary

55,789 *Article* in *Journal* from the *items* table should not have
been indexed in the KB systems, but belong to the *ESCI*. For 2017 these
*ESCI* exclusive items make up about 3% of all *Article* in the *items*
table (*Review*: about 4%). We might exclude them by controlling for
*databasecollection*, but have to keep in mind that the same *pk\_item*
might be assigned to several citations indices or proceedings indices.

Ignoring the *ESCI* issue we might access original *Article* (and
*Review*) in *Journal*, as well as former proceedings published as
*Article* in a *Journal* (i.e. the overlap between articles and
proceedings) by solely controlling for *doctype = ‘Journal’* and
*pubtype IN (‘Article’, ‘Review’)*. The exclusive proceedings,
i.e. without the overlap, might be accessed via *doctype = ‘Proceedings
Paper’* irrespective of the *pubtype*. Likewise proceedings might be
identified in Scopus via *doctype = ‘cp’* without controlling for
*pubtype*.
